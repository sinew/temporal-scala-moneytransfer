import io.temporal.activity.ActivityInterface

@ActivityInterface
trait Account {
  def deposit(accountId: String, referenceId: String, amountCents: Int): Unit

  def withdraw(accountId: String, referenceId: String, amountCents: Int): Unit
}
