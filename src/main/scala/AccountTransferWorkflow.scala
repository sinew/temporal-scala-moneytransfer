import io.temporal.workflow.{WorkflowInterface, WorkflowMethod}

@WorkflowInterface
trait AccountTransferWorkflow {
  @WorkflowMethod
  def transfer(fromAccountId: String, toAccountId: String, referenceId: String, amountCents: Int): Unit
}
