import AccountActivityWorker.TASK_QUEUE
import io.temporal.client.{WorkflowClient, WorkflowOptions}
import io.temporal.serviceclient.WorkflowServiceStubs
import io.temporal.workflow.Functions.Proc4

import java.lang.Integer.parseInt
import java.util.UUID
import scala.util.Random

object TransferRequester {
  def main(args: Array[String]): Unit = {
    val (reference: String, amountCents: Int) = args match {
      case Array(refId, amtCents: String) => (refId, parseInt(amtCents))
      case _ => (UUID.randomUUID().toString, new Random().nextInt(5000))
    }

    val service = WorkflowServiceStubs.newLocalServiceStubs
    // client that can be used to start and signal workflows
    val workflowClient = WorkflowClient.newInstance(service)

    // now we can start running instances of the saga - its state will be persisted
    val options = WorkflowOptions.newBuilder.setTaskQueue(TASK_QUEUE).build
    val transferWorkflow = workflowClient.newWorkflowStub(classOf[AccountTransferWorkflow], options)
    val from = "account1"
    val to = "account2"
    WorkflowClient.start(transferWorkflow.transfer: Proc4[String, String, String, Int], from, to, reference, amountCents)
    System.out.printf("Transfer of %d cents from %s to %s requested", amountCents, from, to)
  }
}
