import AccountActivityWorker.TASK_QUEUE
import io.temporal.client.WorkflowClient
import io.temporal.serviceclient.WorkflowServiceStubs
import io.temporal.worker.WorkerFactory

object AccountTransferWorker {

  def main(args: Array[String]): Unit = {
    // Get worker to poll the common task queue.
    // gRPC stubs wrapper that talks to the local docker instance of temporal service.
    val service = WorkflowServiceStubs.newLocalServiceStubs
    // client that can be used to start and signal workflows
    val client = WorkflowClient.newInstance(service)

    // worker factory that can be used to create workers for specific task queues
    val factory = WorkerFactory.newInstance(client)
    val workerForCommonTaskQueue = factory.newWorker(TASK_QUEUE)
    workerForCommonTaskQueue.registerWorkflowImplementationTypes(classOf[AccountTransferWorkflowImpl])
    // Start all workers created by this factory.
    factory.start()
    System.out.println("Worker started for task queue: " + TASK_QUEUE)
  }

}
