class AccountImpl extends Account {
  override def deposit(accountId: String, referenceId: String, amountCents: Int): Unit = {
    System.out.printf(
      "Withdraw to %s of %d cents requested. ReferenceId=%s\n",
      accountId, amountCents, referenceId)
  }

  override def withdraw(accountId: String, referenceId: String, amountCents: Int): Unit = {
    System.out.printf(
      "Deposit to %s of %d cents requested. ReferenceId=%s\n",
      accountId, amountCents, referenceId);
    //    throw new RuntimeException("simulated");
  }
}
