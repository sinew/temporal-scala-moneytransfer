import io.temporal.client.WorkflowClient
import io.temporal.serviceclient.WorkflowServiceStubs
import io.temporal.worker.WorkerFactory

object AccountActivityWorker {
  final val TASK_QUEUE: String = "AccountTransfer";

//  @SuppressWarnings("CatchAndPrintStackTrace")
  def main(args: Array[String]): Unit = {
    // gRPC stubs wrapper that talks to the local docker instance of temporal service.
    val service = WorkflowServiceStubs.newLocalServiceStubs
    // client that can be used to start and signal workflows
    val client = WorkflowClient.newInstance(service)

    // worker factory that can be used to create workers for specific task queues
    val factory = WorkerFactory.newInstance(client)
    val worker = factory.newWorker(TASK_QUEUE)
    val account = new AccountImpl()
    worker.registerActivitiesImplementations(account)

    // Start all workers created by this factory.
    factory.start()
    System.out.println("Activity Worker started for task queue: " + TASK_QUEUE)
  }
}
