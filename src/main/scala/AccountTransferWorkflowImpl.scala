import io.temporal.activity.ActivityOptions
import io.temporal.workflow.Workflow

import java.time.Duration

class AccountTransferWorkflowImpl extends AccountTransferWorkflow {

  private val options = ActivityOptions.newBuilder.setStartToCloseTimeout(Duration.ofSeconds(5)).build
  private val account = Workflow.newActivityStub(classOf[Account], options)

  override def transfer(fromAccountId: String, toAccountId: String, referenceId: String, amountCents: Int): Unit = {
    account.withdraw(fromAccountId, referenceId, amountCents)
    account.deposit(toAccountId, referenceId, amountCents)
  }
}
