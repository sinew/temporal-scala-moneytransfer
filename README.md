This is a scala version of temporal's [moneytransfer example](https://github.com/temporalio/samples-java/tree/main/src/main/java/io/temporal/samples/moneytransfer)
All credit to them and many thanks for providing examples!

If you're interested more in temporal, check out their [docs](https://docs.temporal.io/)

I also did a writeup of the process of going from java to scala which can be found 
[here](https://sinew.gitlab.io/posts/temporal_scala_moneytransfer/) 