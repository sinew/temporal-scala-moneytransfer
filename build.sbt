ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .settings(
    name := "moneytransfer",
  )

libraryDependencies ++= Seq(
  "io.temporal" % "temporal-sdk" % "1.14.0"
)
